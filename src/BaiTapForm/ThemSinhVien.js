import React, { Component } from "react";
import { connect } from "react-redux";

class ThemSinhVien extends Component {
  state = {
    values:{
    maSV:'',
    hoTen:'',
    soDienThoai:'',
    email:'',
    },
    errors :{
      maSV:'',
      hoTen:'',
      soDienThoai:'',
      email:'',
      }
    
  }
  handleChange =(e)=>{
    let tagInput=e.target;
    let {name,value,type}=tagInput;
    let errorMessage ='';
    //kiem tra rong
    if (value === '') {
      errorMessage = name + 'khong duoc bo trong'
    }
    //kiem tra email
    if(type === 'email'){
      const regex=/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/
      if (!regex.test(value)) {
        errorMessage = name + 'khong dung dinh dang'

      }


    }

    let values={...this.state.values, [name]:value};
    let errors={...this.state.errors, [name]:errorMessage};

    this.setState({
      values:values,
      errors:errors,
    },()=>{
        console.log(this.state);
    })

  }
  handleSubmit =(e) => {
    e.preventDefault();
    this.props.themSinhVien(this.state.values);
  }
  renderButton =()=>{
    let valid=true;
    for (let key in this.state.errors){
      if(this.state.errors[key] !== '')
      {
        valid =false
      } 
    }
      if(valid)
      {
        return  <button onClick={this.handleSubmit} className='btn btn-success'>Them Sinh Vien</button>
      } 
        return   <button onClick={this.handleSubmit} disabled className='btn btn-success'>Them Sinh Vien</button>
    }
  
  render() {
    return (
      <div className="container">
        <div className="card text-white bg-primary">
          <h3>Thong Tin Sinh Vien</h3>
        </div>
        <div className="card-body">
          <from >
            <div className="row">
              <div className="form-group col-6 text-left">
                <span>Ma Sinh Vien</span>
                <input
                  className="form-control"
                  name="maSV"
                  value={this.state.values.maSV}
                  onChange={this.handleChange}
                />
                <p className="text-danger">{this.state.errors.maSV}</p>
              </div>
              <div className="form-group col-6 text-left">
                <span>Ho Ten</span>
                <input
                  className="form-control"
                  name="hoTen"
                  value={this.state.values.hoTen}
                  onChange={this.handleChange}
                />
                <p className="text-danger">{this.state.errors.hoTen}</p>

              </div>
            </div>
            <div className="row">
              <div className="form-group col-6 text-left">
                <span>So Dien Thoai</span>
                <input
                  className="form-control"
                  name="soDienThoai"
                  value={this.state.values.soDienThoai}
                  onChange={this.handleChange}
                />
                <p className="text-danger">{this.state.errors.soDienThoai}</p>

              </div>
              <div className="form-group col-6 text-left">
                <span>Email</span>
                <input
                 type='email'
                  className="form-control"
                  name="email"
                  value={this.state.values.email}
                  onChange={this.handleChange}
                />
                <p className="text-danger">{this.state.errors.email}</p>

              </div>
            </div>
            <div className='row'>
                    <div className='col-md-12 text-align'>
                      {this.renderButton()}
                    </div>
                </div>
          </from>

       
        </div>
      </div>
    );
  }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        themSinhVien:(sinhVien)=>{
            const action= {
                type:'THEM_SINH_VIEN',
                sinhVien
            }
            dispatch(action);
        }
    }


}
export default connect (null,mapDispatchToProps)(ThemSinhVien)