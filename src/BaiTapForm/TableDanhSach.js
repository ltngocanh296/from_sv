import React, { Component } from 'react'
import { connect } from 'react-redux'

class TableDanhSach extends Component {
  renderTbody =()=>{
    return this.props.mangSinhVien.map((sv,index) => { 
        return <tr key={index}>
        <td>{sv.maSV}</td>
        <td>{sv.hoTen}</td>
        <td>{sv.soDienThoai}</td>
        <td>{sv.email}</td>
      </tr>
        
     });

  }
  render() {
    return (
      <div className='container'>
        <table className="table">
          <thead>
            <tr className='bg-primary text-white '>
              <th>ID</th>
              <th>Name</th>
              <th>SDT</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps =(state)=>{
    return {
        mangSinhVien: state.quanLySVReducer.mangSinhVien
    }
}

export default connect(mapStateToProps)(TableDanhSach);
