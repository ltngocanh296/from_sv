import React, { Component } from 'react'
import TableDanhSach from './TableDanhSach'
import ThemSinhVien from './ThemSinhVien'

export default class BaiTapForm extends Component {
  render() {
    return (
      <div>
        <ThemSinhVien />
        <TableDanhSach/>
      </div>
    )
  }
}
